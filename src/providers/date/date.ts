import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {Observable} from 'rxjs/Rx';

@Injectable()
export class DateProvider {
  public cur:any=new Date();
  public end:any=new Date();
  public dif : any;
  public days:any;
  public hours:any;
  public minutes:any;

  constructor(public http: HttpClient) {
    console.log('Hello DateProvider Provider');
  }

  addDays(numDays:number){
    this.end.setDate(this.end.getDate() + numDays);
  }
  resetEnd(numDays:number){
    this.end=new Date();
    this.end.setDate(this.end.getDate() + numDays);
  }
  updateTime(){
   // For me to Understand: get method is asynchronous and it will return an Observable, not the data that is being loaded in from the server.
   // lets suscribe to the obseravble...
    Observable.interval(1000).subscribe(x => {
      this.cur=new Date();
      this.dif=this.end-this.cur;
      this.days=Math.floor((this.dif/1000)/(60*60*24));
      this.hours=Math.floor(((this.dif/1000)%(60*60*24))/(60*60));
      this.minutes=Math.floor(((this.dif/1000)%(60*60))/(60));
      console.log("Cur : "+this.cur);
      console.log("fin : "+this.end);
      console.log("dif : "+this.dif);
      console.log("days : "+this.days);
      console.log("hours : "+this.hours);
      console.log("minutes : "+this.minutes);
      
    });
  }

}