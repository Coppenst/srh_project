var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
// Generate Fake Datas for the Userslist
var DataProvider = (function () {
    function DataProvider() {
        this.my_items = [];
        this.my_items_for_search = [];
        this.current_num = 0;
        console.log('Hello DataProvider Provider');
        this.available_items = [
            { name: "Cable", mine: false },
            { name: "Beamer", mine: false },
            { name: "TV", mine: false },
            { name: "Pen", mine: false },
            { name: "Book", mine: false },
        ];
        this.available_items_for_search = this.available_items;
    }
    DataProvider.max = 3;
    DataProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [])
    ], DataProvider);
    return DataProvider;
}());
export { DataProvider };
//# sourceMappingURL=data.js.map