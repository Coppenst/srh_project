import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
// Generate Fake Datas for the Userslist
@Injectable()
export class DataProvider {
  public available_items: any[];
  public available_items_for_search: any[];
  public my_items:any[]=[];
  public my_items_for_search: any[]=[];
  public current_num:number=0;
  public static max: number=3;
  constructor() {
    console.log('Hello DataProvider Provider');
    this.available_items = [

      { name: "Cable", mine: false, details:"The cable is black and 10 Meters Long. It can be use only for normal power connector.",time:{endDate:Date,days:0,hours:0,minutes:0}}, 
      { name: "Beamer", mine: false, details:"The video projector (beamer) is silver and can be used with VGA or HDMI connectors.",time:{endDate:Date,days:0,hours:0,minutes:0}}, 
      { name: "TV", mine: false, details:"The TV is 50 inch. It supports touch-screen feauter.",time:{endDate:Date,days:0,hours:0,minutes:0}}, 
      { name: "Pen", mine: false, details:"The pen is white and is connected visa USB connection to PCs.",time:{endDate:Date,days:0,hours:0,minutes:0}}, 
      { name: "Book", mine: false, details:"The cable is black and 10 Meters Long. I can be use only for normal power connector",time:{endDate:Date,days:0,hours:0,minutes:0}}, 
      { name: "Laptop", mine: false, details:"The laptop is a Lenovo X1 Carbon system, and contains all the necessary academic software.",time:{endDate:Date,days:0,hours:0,minutes:0}}, 
      { name: "Software", mine: false, details:"The license to software DVDs are available at SRH Microsoft Dreamspark portal.",time:{endDate:Date,days:0,hours:0,minutes:0}}, 
      { name: "Chair", mine: false, details:"The ergonomic chairs are dark-brown and the classroom chairs are silver.",time:{endDate:Date,days:0,hours:0,minutes:0}}, 

      
    ];
    this.available_items_for_search=this.available_items;
  }

}
