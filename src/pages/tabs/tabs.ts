import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ItemListPage } from "../item-list/item-list";
import { ItemsPage } from "../items/items";


/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  avlPage = ItemListPage;
  myItems = ItemsPage;
  my_items:any[];
  available_items:any[];
  constructor(public navCtrl:NavController,public navParams: NavParams){

  }
  ionViewDidLoad(){
    console.log('IonViewDidLoad tabspage');
  }
}
