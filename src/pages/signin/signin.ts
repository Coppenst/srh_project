import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { SignupPage } from '../signup/signup';
import { AuthService } from '../../services/auth';
import { NgForm } from '@angular/forms';
import { UserProfilePage } from '../user-profile/user-profile';
import { AdminUsercontrolPage } from '../admin-usercontrol/admin-usercontrol';
import { AdminAddlistPage } from '../admin-addlist/admin-addlist';
import { MenuPage } from '../menu/menu';
import {FeedbackPage} from '../../pages/feedback/feedback'


/**
 * Generated class for the SigninPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
  
})
export class SigninPage {
//this is the criteria
  criteria:any="";
  constructor(private authService: AuthService, public navCtrl: NavController, 
    public navParams: NavParams, private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) {
  }

  onSignin(form: NgForm){
    const loading = this.loadingCtrl.create({
          content:'Signing you in..'
    });

    loading.present();
    this.authService.signin(form.value.email,form.value.password)

    .then(data =>
    {
      //we take the first part after the @ of the email
      this.criteria=form.value.email.split('@')[1].split('.')[0];
      //we see if its admin or student
      if(this.criteria=="stud"){
        loading.present();
          loading.dismiss();
          console.log("hiii student");
          //we redirect to the userpage
          this.navCtrl.setRoot(MenuPage);

      }
      else if(this.criteria=="admin"){
        loading.dismiss();
        console.log("hiii admin");
         //we redirect to the userpage
        this.navCtrl.setRoot(AdminAddlistPage);
      }
      console.log(data);
    })
    .catch(error =>{
      loading.dismiss();
      const alert = this.alertCtrl.create({
             title: 'Signin failed!',
             message: error.message,
             buttons: ['OK']
      });
      alert.present();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
  }
  signIn(event){
    //this.navCtrl.setRoot(MenuPage);
  }
  goSignUp(event){
    this.navCtrl.setRoot(SignupPage);
  }
  giveFeedback(event){
   
    this.navCtrl.setRoot(FeedbackPage);
  }

  showForgotPassword(){
  
    let promt = this.alertCtrl.create({
     title: 'Enter Your Email',
     message: "A new password will be sent to your Email",
     inputs: [
       {
         name: 'recoverEmail',
         placeholder:'you@example.hochschule-heidelberg.de',
       },
     ],
     buttons: [
       {
         text: 'Cancel',
         handler: data => {
           console.log('cancel clicked');
         }
       },

       {
         text: 'Submit',
         handler: data => {
      
         //add preloader

         
         let loading = this.loadingCtrl.create({
           dismissOnPageChange: true,
           content: 'Reseting your password..',
         });
         loading.present();
          //call user service
         this.authService.forgotPasswordUser(data.recoverEmail).then(()=>{  
             //add toast  
             loading.dismiss().then(()=>{
               //show pop up
               loading.dismiss();
               const alert = this.alertCtrl.create({
                      title: 'Check your email!',
                      message: ' Password reset successful',
                      buttons: ['OK']
            });
            alert.present();

             })
              
            
         }, error =>{
          
            loading.dismiss();
            const alert = this.alertCtrl.create({
                   title: 'Error resetting password!',
                   message: error.message,
                   buttons: ['OK']
         });
         alert.present();
        });

     
         }
       }
     ]

    });
    promt.present();

  }
}
