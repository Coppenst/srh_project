import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ItemsPage } from '../items/items';
/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
  public item:any;
  public again:false;


  constructor(public navCtrl: NavController, public alertCtrl:AlertController, public navParams: NavParams) {
      this.item=navParams.get("item");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }
  rentAgain(event){
    if(this.again){
      if(this.item.count<2){
        this.item.count=this.item.count+1;
      this.item.time.endDate.setDate(this.item.time.endDate.getDate() + 3);
      this.navCtrl.push(ItemsPage,{"item":this.item});
    }
    else{
      let alert = this.alertCtrl.create({
        title: 'Rent failed',
        subTitle: 'You have already rent this item one more time !',
        buttons: ['Ok']
      });
      alert.present();
      this.navCtrl.push(ItemsPage,{"item":this.item});
      }
  }
}
}
