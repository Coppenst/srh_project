import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage({
	name: "manage-document"
})
@Component({
  selector: 'page-manage-document',
  templateUrl: 'manage-document.html',
})
export class ManageDocumentPage {

   public form          : any;
   public records       : any;
   public equipment          : string          = '';
   public status    : string          = '';
   public comment 	: string          = '';
   public docID         : string          = '';
   public isEditable    : boolean         = false;
   public title 		: string		   = 'Add a new document';
   private _COLL 		: string 			= "Equipments Collection";


   constructor(public navCtrl        : NavController,
               public params         : NavParams,
               private _FB 	         : FormBuilder,
               private _DB           : DatabaseProvider,
               private _ALERT        : AlertController)
   {

      this.form 		= _FB.group({
         'equipment' 		        : ['', Validators.required],
         'status' 	        : ['', Validators.required],
         'comment'	        : ['', Validators.required]
      });

      if(params.get('isEdited'))
      {
          let record 		        = params.get('record');

          this.equipment	            = record.location.equipment;
          this.status   	  = record.location.status;
          this.comment      = record.location.comment;
          this.docID            = record.location.id;
          this.isEditable       = true;
          this.title            = 'Update this document';
      }
   }


   saveDocument(val : any) : void
   {
      let equipment	            : string		= this.form.controls["equipment"].value,
	 	      status        : string 		= this.form.controls["status"].value,
  		    comment       : string		= this.form.controls["comment"].value;


      if(this.isEditable)
      {

         this._DB.updateDocument(this._COLL,
                               this.docID,
                               {
	                               equipment    : equipment,
	                               status    : status,
	                               comment   : comment
	                           })
         .then((data) =>
         {
            this.clearForm();
            this.displayAlert('Success', 'The document ' +  equipment + ' was successfully updated');
         })
         .catch((error) =>
         {
            this.displayAlert('Updating document failed', error.message);
         });
      }

      else
      {
         this._DB.addDocument(this._COLL,
                            {
	                           equipment     : equipment,
	                           status    : status,
	                           comment   : comment
	                        })
         .then((data) =>
         {
            this.clearForm();
            this.displayAlert('Record added', 'The document ' +  equipment + ' was successfully added');
         })
         .catch((error) =>
         {
            this.displayAlert('Adding document failed', error.message);
         });
      }
   }

   displayAlert(title      : string,
                message    : string) : void
   {
      let alert : any     = this._ALERT.create({
         title      : title,
         subTitle   : message,
         buttons    : ['Got it!']
      });
      alert.present();
   }

   clearForm() : void
   {
      this.equipment  			= '';
      this.status				= '';
      this.comment 				= '';
   }

}
