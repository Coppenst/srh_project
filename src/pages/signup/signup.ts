import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ActionSheetController } from 'ionic-angular';
// import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthService } from "../../services/auth";
import { SigninPage } from '../signin/signin';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  // Inject the auth.Service
  constructor(private  authService: AuthService
            , private loadingCtrl: LoadingController
            , private alertCtrl: AlertController
            , public navCtrl: NavController
            , public actionSheetCtrl: ActionSheetController
             ) {}
  

  onSignup(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content:'Registration in process..'
    });
    
    loading.present();
    
    this.authService.signup(form.value.email, form.value.password)
      .then(data => {
        loading.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Congratulations!',
          message: 'Registration successful!',
          buttons: ['OK']
   });
   alert.present();
        
      })

      .catch(error =>{
        loading.dismiss();
        const alert = this.alertCtrl.create({
               title: 'Oops! Registration failed!',
               message: error.message,
               buttons: ['OK']
        });
        alert.present();
      });
  }
  goSignIn(event){
    this.navCtrl.setRoot(SigninPage);
  }

  termsAndConditionsSheet(){
    const loading = this.loadingCtrl.create({
    });
    loading.dismiss();
    const alert = this.alertCtrl.create({
           title: 'Terms and Conditions',
           message: '1. TERMS OF USE<br /><br />By downloading, browsing, accessing or using this Sample Store mobile or web application (<b>“SRH Equipment Library”</b>), you agree to be bound by these Terms and Conditions of Use. We reserve the right to amend these terms and conditions at any time. If you disagree with any of these Terms and Conditions of Use, you must immediately discontinue your access to the Mobile Application and your use of the services offered on the Mobile Application. Continued use of the Mobile Application or the website will constitute acceptance of these Terms and Conditions of Use, as may be amended from time to time.<br /><br />2. DEFINITIONS<br /><br />In these Terms and Conditions of Use, the following capitalised terms shall have the following meanings, except where the context otherwise requires:<br /><br /><b>"Account"</b> means an account created by a “user” as part of Registration.<br /><br /><b>"Privacy Policy"</b> means the privacy policy set out in Clause 14 of these Terms and Conditions of Use.<br /><br /><b>"Equipment"</b> means the service or object which is borrowed using this application.<br /><br /><b>"Register"</b> means to create an Account on the Mobile Application and "Registration"</b> means the act of creating such an Account.<br /><br /><b>"Services"</b> means all the services provided by “SRH Equipment Library” via the mobile application or web to the users, and "Service"</b> means any one of them,<br /><br /><b>"Users"</b> means users of the “SRH Equipment Library” application (web or mobile), including you and "User"</b> means any one of them.<br /><br /><b>"Overdue Fees"</b> means the fees that the users might be charged after a borrowed item is kept past its due date.<br /><br /><br />3. GENERAL ISSUES SRH EQUIPMENT LIBRARY AND THE SERVICES<br /><br />3.1 The application, the accounts and the equipments are a property of the SRH University of Applied Science Heidelberg (Hochschule SRH Heidelberg).<br /><br />3.2 The overdue fees are mandatory fees which any user must pay before the end of their contract with SRH University of Applied Science Heidelber.<br /><br />4. PERMISSIONS<br /><br />4.1 Users agree that they do not claim the ownership of the application, their accounts or the the equipments; thus cannot share them with other users.<br /><br />4.2 SRH University of Applied Science Heidelber reserves any rights to make any decisions such as overdue fines, banning users and etc. in case of disputes.',
           buttons: ['OK']
    });
    alert.present();
   }
}