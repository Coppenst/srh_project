var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
var ItemListPage = (function () {
    function ItemListPage(navCtrl, navParams, data, toastCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = data;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.available_items = data.available_items;
        this.available_items_for_search = data.available_items;
        this.my_items = data.my_items;
    }
    ItemListPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ItemListPage');
    };
    // Generate Function to Rent a Item
    ItemListPage.prototype.rentItem = function (event) {
        var _this = this;
        this.countToRent();
        if (this.toRent > 0) {
            if ((this.data.current_num + this.toRent) <= DataProvider.max) {
                var alert_1 = this.alertCtrl.create({
                    title: 'Confirmation',
                    message: 'Are you sure to rent the selected items?',
                    buttons: [
                        {
                            text: 'No',
                            role: 'cancel',
                            handler: function () {
                                console.log('Cancel clicked');
                            }
                        },
                        {
                            text: 'Yes',
                            handler: function () {
                                _this.executeRent();
                            }
                        }
                    ]
                });
                alert_1.present();
            }
            else {
                var toast = this.toastCtrl.create({
                    message: "You can not have more than 3 items !",
                    duration: 1500
                });
                toast.present();
            }
        }
    };
    ItemListPage.prototype.executeRent = function () {
        var _this = this;
        var temp = [];
        this.available_items_for_search.forEach(function (element) {
            if (element.mine == true) {
                _this.my_items.push(element);
                _this.data.current_num++;
            }
            else {
                temp.push(element);
            }
        });
        this.available_items = temp;
        this.available_items_for_search = temp;
        this.data.available_items = temp;
        this.data.available_items_for_search = temp;
    };
    // Let the Items Clickable
    ItemListPage.prototype.showArrays = function (event) {
        console.log(this.available_items);
        console.log(this.my_items);
    };
    // Add items to MyItems List. 
    ItemListPage.prototype.updateList = function (event) {
        var _this = this;
        console.log("test");
        console.log(this.myInput);
        var temp = [];
        this.available_items_for_search.forEach(function (element) {
            if (element.name == _this.myInput) {
                temp.push(element);
            }
        });
        this.available_items = temp;
        if (this.myInput == "") {
            this.available_items = this.available_items_for_search;
        }
    };
    ItemListPage.prototype.countToRent = function () {
        var _this = this;
        this.toRent = 0;
        this.available_items_for_search.forEach(function (element) {
            if (element.mine == true) {
                _this.toRent++;
            }
        });
    };
    ItemListPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-item-list',
            templateUrl: 'item-list.html',
        })
        // Generate a Class with Attributes for the Itemslist
        ,
        __metadata("design:paramtypes", [NavController, NavParams, DataProvider, ToastController, AlertController])
    ], ItemListPage);
    return ItemListPage;
}());
export { ItemListPage };
//# sourceMappingURL=item-list.js.map