import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs, ToastController, AlertController } from 'ionic-angular';
import { ItemsPage } from '../items/items';
import { DataProvider } from '../../providers/data/data';



@IonicPage()
@Component({
  selector: 'page-item-list',
  templateUrl: 'item-list.html',
})
// Generate a Class with Attributes for the Itemslist
export class ItemListPage {

   public myInput : String;
   public available_items: any[];
   public available_items_for_search: any[];
   public my_items:any[];
   public toRent:number;
  constructor(public navCtrl: NavController, public navParams: NavParams,public data:DataProvider,public toastCtrl: ToastController,private alertCtrl: AlertController) {
  
    this.available_items=data.available_items;
    this.available_items_for_search=data.available_items;
    this.my_items=data.my_items;
    
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ItemListPage');
  }

  // Generate Function to Rent a Item
  rentItem(event) {
    this.countToRent();
    if(this.toRent>0){
        if((this.data.current_num+this.toRent)<=DataProvider.max ){
                let alert = this.alertCtrl.create({
                  title: 'Confirmation',
                  message: 'Are you sure to borrow the selected items?',
                  buttons: [
                    {
                      text: 'No',
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    },
                    {
                      text: 'Yes',
                      handler: () => {
                          this.executeRent();
                      }
                    }
                  ]
                });
                alert.present();
              }

        else{
              let toast = this.toastCtrl.create({
                message: `You can not have more than 3 items !`,
                duration: 1500
                });
              toast.present();
            }
  }


  }
  

  executeRent(){
        let temp: any[]=[];
        this.available_items_for_search.forEach(element => {
          if(element.mine==true){
            let cur=new Date();
              element.time.endDate=new Date();
              element.time.endDate.setDate(cur.getDate() + 3);
              this.my_items.push(element);
              this.data.current_num++;
          }
          else{
              temp.push(element);
          }
        });
        this.available_items=temp;
        this.available_items_for_search=temp; 
        this.data.available_items=temp;
        this.data.available_items_for_search=temp;
  }
  // Let the Items Clickable
  showArrays(event){
    console.log(this.available_items);
    console.log(this.my_items);
  }
  // Add items to MyItems List. 
  updateList(event){
    console.log("test");
    console.log(this.myInput);
    
    let temp : any[] =[];
    this.available_items_for_search.forEach(element => {
      if(element.name==this.myInput){
        temp.push(element);
      }
    });
      this.available_items=temp;
  
    
    if(this.myInput==""){
        this.available_items=this.available_items_for_search;
    }
  }
  countToRent(){
    this.toRent=0;
    this.available_items_for_search.forEach(element => {
      if(element.mine==true){
        this.toRent++;
      }
    });
  }  

}



