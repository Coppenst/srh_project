import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { FormsModule } from '@angular/forms';

interface Inventory{
  id:string;
  item: string;
  description: string;
}
@IonicPage()


@Component({
  selector: 'page-inventory-list',
  templateUrl: 'inventory-list.html',
})
export class InventoryListPage {


  item:string;
  description:string;
  itemid: any;
  
  editState:boolean=false;
  itemToEdit:Inventory;
  
  inventoryDoc: AngularFirestoreDocument<Inventory>;
  inventory: Observable<Inventory>;
  
    inventoryCollection: AngularFirestoreCollection<Inventory>;
    inventorys: Observable<Inventory[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams,private afs: AngularFirestore) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InventoryListPage');
  }
  ngOnInit(){
    this.inventoryCollection= this.afs.collection('inventory');
    //this.inventorys=this.inventoryCollection.valueChanges();
  
    this.inventorys=this.inventoryCollection.snapshotChanges().map(actions =>{
      return actions.map(a => {
        const data= a.payload.doc.data() as Inventory;
         data.id = a.payload.doc.id;
        console.log("Item Id is : ",data.id);
      
        return(data);
      })
    })
  }

  itemSelected(event,inventory){
    console.log("Selected Item", inventory.description);
   
    this.editState=true;
    this.itemToEdit=inventory;
    console.log(this.itemToEdit.id +" id in item selected");
   }
   addItems(){
   this.afs.collection('inventory').add({'item':this.item, 'description':this.description});
   this.description =null;
   this.item=null;
   }
   deleteItem(ItmeId){
     this.clearState();
   this.afs.doc('inventory/'+ItmeId).delete();
   }
   
   clearState(){
     this.editState=false;
     this.itemToEdit=null;
   }
   
   
}
