import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MenuPage } from '../menu/menu';
import {TabsPage  } from '../tabs/tabs';
/**
 * Generated class for the GuidlinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-guidline',
  templateUrl: 'guidline.html',
})
export class GuidlinePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GuidlinePage');
  }

  goBack(event){
    this.navCtrl.setRoot(MenuPage);
  }
}
