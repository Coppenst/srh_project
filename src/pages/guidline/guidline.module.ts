import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GuidlinePage } from './guidline';

@NgModule({
  declarations: [
    GuidlinePage,
  ],
  imports: [
    IonicPageModule.forChild(GuidlinePage),
  ],
})
export class GuidlinePageModule {}
