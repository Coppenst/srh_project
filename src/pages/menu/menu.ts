import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, AlertController } from 'ionic-angular';
import { SigninPage } from '../signin/signin';
import { TabsPage } from '../tabs/tabs';
import { SignupPage } from '../signup/signup';
import { AuthService } from '../../services/auth';
import { GuidlinePage } from '../guidline/guidline';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {


  @ViewChild('content') childNavCtrl: NavController;
  public signinPage:typeof SigninPage;
  public tabsPage:typeof TabsPage;
  public signupPage:typeof SignupPage;
  public guidlinePage:typeof GuidlinePage;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private menuCtrl: MenuController, private authservice: AuthService, 
  private loadingCtrl: LoadingController,private alertCtrl: AlertController ) {
    this.signinPage=SigninPage;
    this.signupPage=SignupPage;
    this.tabsPage=TabsPage;
    this.guidlinePage=GuidlinePage;
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }
  onLoad(page: any){
    this.childNavCtrl.setRoot(page);
    this.menuCtrl.close();
  }


  OnLogout(){
    const loading = this.loadingCtrl.create({
      content:' Logging you out........'
});

    loading.present();
    loading.present();
    loading.dismiss();
    this.authservice.logout();
    this.menuCtrl.close();
    this.navCtrl.setRoot(SigninPage);  
 
  }

}
