import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { DetailsPage } from '../details/details';
import {Observable} from 'rxjs/Rx';
/**
 * Generated class for the ItemsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-items',
  templateUrl: 'items.html',
})
export class ItemsPage {

  public myInput : String;
  public my_items:any[];
  public my_items_for_search: any[]=[];

    
  constructor(public navCtrl: NavController, public navParams: NavParams, public data:DataProvider) {
    this.my_items=data.my_items;
    this.my_items_for_search=data.my_items;
    this.watchTime();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ItemsPage');
    let item:any;
    item=this.navParams.get("item");
    if(item!=null){
      console.log("coucouuu!!!");
      this.my_items_for_search.forEach(e =>{
          if(e.name==item.name){
            e=item;
          }
      });
  }
  }

  updateList(event){
    console.log("test");
    console.log(this.myInput);

    let temp: any[]=[];
    this.my_items_for_search.forEach(element =>{
      if(element.name==this.myInput){
        temp.push(element);
      }
      });
      this.my_items=temp;

      if(this.myInput==""){
        this.my_items=this.my_items_for_search;
      }
  }
  watchTime(){
    
    Observable.interval(1000).subscribe(x => {
          this.my_items_for_search.forEach(e =>{
          var cur: any=new Date();
          let dif=e.time.endDate-cur;
          e.time.days=Math.floor((dif/1000)/(60*60*24));
          e.time.hours=Math.floor(((dif/1000)%(60*60*24))/(60*60));
          e.time.minutes=Math.floor(((dif/1000)%(60*60))/(60));
        });
    });
}
  // Pushs me to the Details Page. The Click function for that is in the Items.html
  // added some small animation after clicking on a single Item
  goDetails(item){

    const animationsOptions = {
      animation: 'md-transition',
      duration: 1000
    }
    this.navCtrl.push(DetailsPage,{"item":item}, animationsOptions);
  }
}
