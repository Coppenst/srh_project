var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
/**
 * Generated class for the ItemsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ItemsPage = (function () {
    function ItemsPage(navCtrl, navParams, data) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = data;
        this.my_items_for_search = [];
        this.my_items = data.my_items;
        this.my_items_for_search = data.my_items;
    }
    ItemsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ItemsPage');
    };
    ItemsPage.prototype.updateList = function (event) {
        var _this = this;
        console.log("test");
        console.log(this.myInput);
        var temp = [];
        this.my_items_for_search.forEach(function (element) {
            if (element.name == _this.myInput) {
                temp.push(element);
            }
        });
        this.my_items = temp;
        if (this.myInput == "") {
            this.my_items = this.my_items_for_search;
        }
    };
    ItemsPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-items',
            templateUrl: 'items.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, DataProvider])
    ], ItemsPage);
    return ItemsPage;
}());
export { ItemsPage };
//# sourceMappingURL=items.js.map