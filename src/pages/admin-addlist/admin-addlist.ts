import { Component } from '@angular/core';
import { NavController, AlertController, MenuController, LoadingController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { InventoryListPage } from '../inventory-list/inventory-list';
import { SigninPage } from '../signin/signin';
import { AuthService } from '../../services/auth';

/**
 * Generated class for the AdminAddlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-admin-addlist',
  templateUrl: 'admin-addlist.html',
})
export class AdminAddlistPage {

   private _COLL 		: string 			= "Equipments Collection";
   private _DOC 		: string 			= "Xy76Re34SdFR1";
   private _CONTENT  	: any;
   public locations     : any;

   constructor(public navCtrl  : NavController,
               private _DB     : DatabaseProvider,
               private _ALERT  : AlertController, private menuCtrl: MenuController, private authservice: AuthService, 
               private loadingCtrl: LoadingController)
   {
      this._CONTENT = {
         equipment 			: "Pen Drive",
         status 	: "Rented",
         comment    : "Must return in 3 days"
      };
   }

   ionViewDidEnter()
   {
      this.retrieveCollection();
   }

   OnLogout(){
    const loading = this.loadingCtrl.create({
      content:' Logging you out........'
});

    loading.present();
    loading.present();
    loading.dismiss();
    this.authservice.logout();
    this.menuCtrl.close();
    this.navCtrl.setRoot(SigninPage);  
 
  }

   generateCollectionAndDocument() : void
   {
      this._DB.createAndPopulateDocument(this._COLL,
                                         this._DOC,
                                         this._CONTENT)
      .then((data : any) =>
      {
         console.dir(data);
      })
      .catch((error : any) =>
      {
         console.dir(error);
      });
   }

   retrieveCollection() : void
   {
      this._DB.getDocuments(this._COLL)
      .then((data) =>
      {
         if(data.length === 0)
         {
            this.generateCollectionAndDocument();
         }

         else
         {
            this.locations = data;
         }
      })
      .catch();
   }

   addDocument() : void
   {
      this.navCtrl.push('manage-document');
   }

   updateDocument(obj) : void
   {
      let params : any = {
         collection   : this._COLL,
         location     : obj
      };
      this.navCtrl.push('manage-document', { record : params, isEdited : true });
   }

   deleteDocument(obj) : void
   {
      this._DB.deleteDocument(this._COLL,
      						obj.id)
      .then((data : any) =>
      {
         this.displayAlert('Success', 'The record ' + obj.equipment + ' was successfully removed');
      })
      .catch((error : any) =>
      {
         this.displayAlert('Error', error.message);
      });
   }

   displayAlert(title      : string,
                message    : string) : void
   {
      let alert : any     = this._ALERT.create({
         title      : title,
         subTitle   : message,
         buttons    : [{
          text      : 'Got It!',
          handler   : () =>
          {
            this.retrieveCollection();
          }
        }]
      });
      alert.present();
   }
 
   equipmentInvenorty(){
     this.navCtrl.setRoot(InventoryListPage);
   }
}
