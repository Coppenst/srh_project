import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminAddlistPage } from './admin-addlist';

@NgModule({
  declarations: [
    AdminAddlistPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminAddlistPage),
  ],
})
export class AdminAddlistPageModule {}
