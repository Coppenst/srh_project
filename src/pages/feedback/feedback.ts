import { IonicPage, NavController, NavParams,AlertController,LoadingController, Loading } from 'ionic-angular';
import {firestore}from 'firebase';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { FormsModule } from '@angular/forms';
import { Component } from '@angular/core';
import { SigninPage } from '../signin/signin';




interface Fb{
email: string;
feedback: string;
}
@IonicPage()
@Component({
selector: 'page-feedback',
templateUrl: 'feedback.html',
})
export class FeedbackPage {

feedback: string;
email: string;

feedbackCollection: AngularFirestoreCollection<Fb>;
feedbackObs: Observable<Fb[]>;



constructor(public navCtrl: NavController, public navParams: NavParams, private alertContrl: AlertController, private loadingControler: LoadingController, private afs: AngularFirestore) {
}
ngOnInit(){


 


this.feedbackCollection= this.afs.collection('feedback');
this.feedbackObs =this.feedbackCollection.valueChanges();

}

ionViewDidLoad() {
console.log('ionViewDidLoad FeedbackPage');
}

test(){
console.log("test");
}


submit(){
    console.log("email", this.email.length);
    
    if (this.email.length > 0 && this.feedback.length >0) {
        const loading = this.loadingControler.create({
            content:'Submitting your feedback..'
      });
      loading.present();
      this.afs.collection('feedback').add({ 'email': this.email, 'feedback':this.feedback})
loading.dismiss();
const alert = this.alertContrl.create({
    title: 'Your feedback is submitted!!',
    buttons: ['OK']
});
alert.present();
this.navCtrl.setRoot(SigninPage);
    } else {
        console.log("else loop");
    }
};

cancle(){
    this.navCtrl.setRoot(SigninPage);
}

}
