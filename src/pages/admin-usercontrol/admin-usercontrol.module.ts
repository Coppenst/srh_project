import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminUsercontrolPage } from './admin-usercontrol';

@NgModule({
  declarations: [
    AdminUsercontrolPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminUsercontrolPage),
  ],
})
export class AdminUsercontrolPageModule {}
