import { Component } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';

//import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ItemsPage } from '../pages/items/items';
import { ItemListPage } from '../pages/item-list/item-list';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import { MenuPage } from '../pages/menu/menu';
import { AuthService } from "../services/auth";
import { AdminAddlistPage } from '../pages/admin-addlist/admin-addlist';
import { GuidlinePage } from '../pages/guidline/guidline';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
 
  rootPage: any = SigninPage;
  isAuthenticated = false;


  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    
    // var config = {
    //   apiKey: "AIzaSyBXhB-9xj3LU-7xhgZm9SopAjDTtRjX7zA",
    //   authDomain: "srh-project-2018.firebaseapp.com",
    //   databaseURL: "https://srh-project-2018.firebaseio.com",
    //   projectId: "srh-project-2018",
    //   storageBucket: "srh-project-2018.appspot.com",
    //   messagingSenderId: "422765855299"
    // };

    var config = {
      apiKey: "AIzaSyC-UWRcDNs-NsZ0OtOs63uMz4nav7__P6U",
      authDomain: "srh-project-36967.firebaseapp.com",
      databaseURL: "https://srh-project-36967.firebaseio.com",
      projectId: "srh-project-36967",
      storageBucket: "srh-project-36967.appspot.com",
      messagingSenderId: "613364871680"
    };
    firebase.initializeApp(config);
  
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.isAuthenticated = true;
      }

    });
    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  // Navigate the Pages i want.... refeernce viewchild because injection doesnt work
 
}

