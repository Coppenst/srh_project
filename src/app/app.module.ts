import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { TabsPage } from "../pages/tabs/tabs";
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { EditItemPage } from '../pages/edit-item/edit-item';
import { ItemPage } from '../pages/item/item';
import { ItemsPage } from '../pages/items/items';
import { ItemListPage } from '../pages/item-list/item-list';
import { DataProvider } from '../providers/data/data';
import { SigninPage } from '../pages/signin/signin';
import { SignupPageModule } from '../pages/signup/signup.module';
import { SignupPage } from '../pages/signup/signup';
import { AuthService } from '../services/auth';
import { MenuPage } from '../pages/menu/menu';
import { DetailsPage } from '../pages/details/details';
import { AdminAddlistPage } from '../pages/admin-addlist/admin-addlist';
import { AdminUsercontrolPage } from '../pages/admin-usercontrol/admin-usercontrol';
import { UserProfilePage } from '../pages/user-profile/user-profile';
import { DateProvider } from '../providers/date/date';
import { DatabaseProvider } from '../providers/database/database';
import { GuidlinePage } from '../pages/guidline/guidline';
import{ InventoryListPage} from '../pages/inventory-list/inventory-list'
import{FeedbackPage} from '../pages/feedback/feedback';

import{AngularFireModule} from 'angularfire2';
import{AngularFirestoreModule} from 'angularfire2/firestore';
import{ FormsModule,ReactiveFormsModule} from '@angular/forms';



var config = {
  apiKey: "AIzaSyC-UWRcDNs-NsZ0OtOs63uMz4nav7__P6U",
  authDomain: "srh-project-36967.firebaseapp.com",
  databaseURL: "https://srh-project-36967.firebaseio.com",
  projectId: "srh-project-36967",
  storageBucket: "srh-project-36967.appspot.com",
  messagingSenderId: "613364871680"
};
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    EditItemPage,
    ItemPage,
    ItemsPage,
    ItemListPage,
    SigninPage,
    SignupPage,
    MenuPage,
    DetailsPage,
    AdminAddlistPage,
    AdminUsercontrolPage,
    UserProfilePage,
    GuidlinePage,
    InventoryListPage,
    FeedbackPage
 
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config),  
    AngularFirestoreModule,
    FormsModule,
    ReactiveFormsModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    EditItemPage,
    ItemPage,
    ItemsPage,
    ItemListPage,
    SigninPage,
    SignupPage,
    MenuPage,
    DetailsPage,
    AdminAddlistPage,
    AdminUsercontrolPage,
    UserProfilePage,
    GuidlinePage,
    InventoryListPage,
    FeedbackPage
  

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider,
    AuthService,
    DateProvider,
    DatabaseProvider,
    
  ]
})
export class AppModule {}
