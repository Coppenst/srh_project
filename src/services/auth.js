import firebase from 'firebase';
var AuthService = (function () {
    function AuthService() {
    }
    // Returns the result of the request
    AuthService.prototype.signup = function (email, password) {
        return firebase.auth().createUserWithEmailAndPassword(email, password);
    };
    AuthService.prototype.signin = function (email, password) {
        return firebase.auth().signInWithEmailAndPassword(email, password);
    };
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.js.map