import firebase from 'firebase';

export class AuthService {
    // Returns the result of the request
    signup(email: string, password: string){
        return firebase.auth().createUserWithEmailAndPassword(email, password);
    }
    signin(email: string, password: string){
        return firebase.auth().signInWithEmailAndPassword(email, password);
    }
    logout(){
        firebase.auth().signOut();

    }

    forgotPasswordUser(email: any){
     return firebase.auth().sendPasswordResetEmail(email);

    }
}